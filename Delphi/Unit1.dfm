object Form1: TForm1
  Left = 218
  Top = 169
  Width = 870
  Height = 668
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 16
    Width = 115
    Height = 19
    Caption = 'Enter size of matrix'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 544
    Top = 16
    Width = 123
    Height = 19
    Caption = 'Calculate determinat'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 184
    Width = 20
    Height = 19
    Caption = 'A='
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object MatrixSizeEdit: TEdit
    Left = 40
    Top = 48
    Width = 41
    Height = 21
    TabOrder = 0
  end
  object GenerateMatrixButton: TButton
    Left = 96
    Top = 48
    Width = 89
    Height = 25
    Caption = 'Generate Matrix'
    TabOrder = 1
  end
  object SingleThreadModeButton: TButton
    Left = 480
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Single Thread'
    TabOrder = 2
  end
  object MultiThreadModeButton: TButton
    Left = 648
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Multi Thread'
    TabOrder = 3
  end
  object DeterminantDBGrid: TDBGrid
    Left = 480
    Top = 88
    Width = 241
    Height = 41
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        Title.Caption = 'det(A)'
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 'Time'
        Visible = True
      end>
  end
  object MatrixDBGrid: TDBGrid
    Left = 40
    Top = 192
    Width = 409
    Height = 225
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
end
