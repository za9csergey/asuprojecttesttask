﻿namespace AsuProjectTestTask.Frontend
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.MatrixSizeTextBox = new System.Windows.Forms.TextBox();
            this.generateMatrixButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SingleThreadMode = new System.Windows.Forms.Button();
            this.MultiThreadButton = new System.Windows.Forms.Button();
            this.DeterminantDGV = new System.Windows.Forms.DataGridView();
            this.determinantValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.determinantInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MatrixLabel = new System.Windows.Forms.Label();
            this.MatrixDGV = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DeterminantDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.determinantInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatrixDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter size of matrix";
            // 
            // MatrixSizeTextBox
            // 
            this.MatrixSizeTextBox.Location = new System.Drawing.Point(28, 32);
            this.MatrixSizeTextBox.Name = "MatrixSizeTextBox";
            this.MatrixSizeTextBox.Size = new System.Drawing.Size(44, 20);
            this.MatrixSizeTextBox.TabIndex = 1;
            // 
            // generateMatrixButton
            // 
            this.generateMatrixButton.Location = new System.Drawing.Point(96, 32);
            this.generateMatrixButton.Name = "generateMatrixButton";
            this.generateMatrixButton.Size = new System.Drawing.Size(115, 23);
            this.generateMatrixButton.TabIndex = 2;
            this.generateMatrixButton.Text = "Genearate matrix";
            this.generateMatrixButton.UseVisualStyleBackColor = true;
            this.generateMatrixButton.Click += new System.EventHandler(this.GenerateMatrixButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(589, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Calculate determinant";
            // 
            // SingleThreadMode
            // 
            this.SingleThreadMode.Location = new System.Drawing.Point(502, 31);
            this.SingleThreadMode.Name = "SingleThreadMode";
            this.SingleThreadMode.Size = new System.Drawing.Size(132, 23);
            this.SingleThreadMode.TabIndex = 4;
            this.SingleThreadMode.Text = "Single thread mode";
            this.SingleThreadMode.UseVisualStyleBackColor = true;
            this.SingleThreadMode.Click += new System.EventHandler(this.SingleThreadMode_Click);
            // 
            // MultiThreadButton
            // 
            this.MultiThreadButton.Location = new System.Drawing.Point(685, 32);
            this.MultiThreadButton.Name = "MultiThreadButton";
            this.MultiThreadButton.Size = new System.Drawing.Size(132, 23);
            this.MultiThreadButton.TabIndex = 5;
            this.MultiThreadButton.Text = "Multi thread mode";
            this.MultiThreadButton.UseVisualStyleBackColor = true;
            this.MultiThreadButton.Click += new System.EventHandler(this.MultiThreadButton_Click);
            // 
            // DeterminantDGV
            // 
            this.DeterminantDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DeterminantDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.determinantValue,
            this.time});
            this.DeterminantDGV.Location = new System.Drawing.Point(531, 61);
            this.DeterminantDGV.Name = "DeterminantDGV";
            this.DeterminantDGV.Size = new System.Drawing.Size(244, 86);
            this.DeterminantDGV.TabIndex = 6;
            // 
            // determinantValue
            // 
            this.determinantValue.HeaderText = "det(A)";
            this.determinantValue.Name = "determinantValue";
            this.determinantValue.ReadOnly = true;
            // 
            // time
            // 
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // determinantInfoBindingSource
            // 
            this.determinantInfoBindingSource.DataSource = typeof(AsuProjectTestTask.MatrixDeterminant.DeterminantInfo);
            // 
            // MatrixLabel
            // 
            this.MatrixLabel.AutoSize = true;
            this.MatrixLabel.Location = new System.Drawing.Point(17, 193);
            this.MatrixLabel.Name = "MatrixLabel";
            this.MatrixLabel.Size = new System.Drawing.Size(20, 13);
            this.MatrixLabel.TabIndex = 7;
            this.MatrixLabel.Text = "A=";
            // 
            // MatrixDGV
            // 
            this.MatrixDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MatrixDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.MatrixDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MatrixDGV.Location = new System.Drawing.Point(43, 172);
            this.MatrixDGV.Name = "MatrixDGV";
            this.MatrixDGV.Size = new System.Drawing.Size(240, 150);
            this.MatrixDGV.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 536);
            this.Controls.Add(this.MatrixDGV);
            this.Controls.Add(this.MatrixLabel);
            this.Controls.Add(this.DeterminantDGV);
            this.Controls.Add(this.MultiThreadButton);
            this.Controls.Add(this.SingleThreadMode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.generateMatrixButton);
            this.Controls.Add(this.MatrixSizeTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DeterminantDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.determinantInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MatrixDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MatrixSizeTextBox;
        private System.Windows.Forms.Button generateMatrixButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SingleThreadMode;
        private System.Windows.Forms.Button MultiThreadButton;
        private System.Windows.Forms.DataGridView DeterminantDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn determinantValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.Label MatrixLabel;
        private System.Windows.Forms.DataGridView MatrixDGV;
        private System.Windows.Forms.BindingSource determinantInfoBindingSource;
    }
}