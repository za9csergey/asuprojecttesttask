﻿using System;
using System.Collections.Generic;

namespace AsuProjectTestTask.MatrixDeterminant
{
    /// <summary>
    /// Represents multiplication: a1j * (-1)^(1+j) * A1j
    /// </summary>
    public class Part
    {
        private readonly double coefficient;    // a1j * (-1)^(1+j)
        private readonly double[,] subMatrix;  // matrix for complementary minor of this part
        private readonly int size;   // size of sub matrix

        /// <summary>
        /// Creates instanse of part
        /// </summary>
        /// <param name="coefficient">a1j * (-1)^(1+j)</param>
        /// <param name="subMatrix">complementary minor</param>
        public Part(double coefficient, double[,] subMatrix)
        {
            this.coefficient = coefficient;
            this.subMatrix = subMatrix;
            size = subMatrix.GetLength(0);
        }

        // proccess this Part 
        public double ProccesPart()
        {
            // if coefficien equls 0 result of this part equals 0 and exit form method
            if (Math.Abs(coefficient) < 0.000000001)
            {
                return 0.0;
            }
            // if matrix is 2x2 calculate result
            if (size == 2)
            {
                return CalculateResultWithMinor();
            }

            // if complementary minor in NxN find his determinant
            double sum = 0.0;
            for (int collumn = 0; collumn < size; collumn++)
            {
                sum += SubMatrix.CalculateSubMatrixDeterminant(subMatrix, collumn);
            }
            // calculate result of this part with coefficient a1j 
            double result = coefficient * sum;
            return result;

        }

        // calculate reuslt of this Part by calculating minor
        private double CalculateResultWithMinor()
        {
            double result = coefficient * (subMatrix[0, 0] * subMatrix[1, 1] -
                                    subMatrix[0, 1] * subMatrix[1, 0]);
            return result;

        }
    }
}