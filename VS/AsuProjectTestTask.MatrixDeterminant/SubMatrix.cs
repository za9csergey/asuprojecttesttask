﻿using System;

namespace AsuProjectTestTask.MatrixDeterminant
{
    public static class SubMatrix
    {
        /// <summary>
        /// Creates sub matrix form input matrix
        /// </summary>
        /// <param name="matrix">input matrix</param>
        /// <param name="collumnToDelete">zaro-based index of collumn to delete in input matrix</param>
        /// <returns>matrix without first row and specified collumn</returns>
        public static double[,] Create(double[,] matrix, int collumnToDelete)
        {
            int size = matrix.GetLength(0);
            double[,] subMatrix = new double[size - 1, size - 1];
            // copy data from matrix except first row and collumnToDelete
            for (int row = 1; row < size; row++)
            {
                int currentCollumn = 0;
                for (int collumn = 0; collumn < size; collumn++)
                {
                    if (collumn != collumnToDelete)
                    {
                        subMatrix[row - 1, currentCollumn] = matrix[row, collumn];
                        currentCollumn++;
                    }
                }
            }
            return subMatrix;
        }

        /// <summary>
        /// Calculates determinant of sub matrix by expansion on the first line
        /// </summary>
        /// <param name="inputMatrix"></param>
        /// <param name="collumnToDelete"></param>
        /// <returns></returns>
        public static double CalculateSubMatrixDeterminant(double[,] inputMatrix, int collumnToDelete)
        {
            // calculate a1j * (-1)^(i+j) : i = 1, j = collumnToDelete
            double coefficient = inputMatrix[0, collumnToDelete] * Math.Pow(-1, 2 + collumnToDelete);
            // get sub matrix
            double[,] subMatrix = Create(inputMatrix, collumnToDelete);
            // create part instance
            Part part = new Part(coefficient, subMatrix);
            // procces part and return result of this part
            return part.ProccesPart(); ;
        }
    }
}
