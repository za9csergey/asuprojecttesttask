﻿using System.Runtime.InteropServices;

namespace AsuProjectTestTask.MatrixDeterminant
{
    /// <summary>
    /// DeterminantInfo struct store determinant info determinant value and time to calculate it
    /// </summary>
    [Guid("4d8c0af1-c6ff-4bc3-a439-c5b9c9207c59")]
    [ComVisible(true)]
    public class DeterminantInfo
    {
        public double Determinant;
        public long TimeToCalculate;

        public DeterminantInfo()
        {
        }

        /// <summary>
        /// Represents determinant calculation info
        /// </summary>
        /// <param name="determinant">value of determinant</param>
        /// <param name="timeToCalculate">time passed to calculate determinant</param>
        public DeterminantInfo(double determinant, long timeToCalculate)
        {
            Determinant = determinant;
            this.TimeToCalculate = timeToCalculate;
        }
    }
}
