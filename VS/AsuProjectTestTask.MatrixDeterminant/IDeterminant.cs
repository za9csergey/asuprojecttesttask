﻿using System.Runtime.InteropServices;

namespace AsuProjectTestTask.MatrixDeterminant
{
    [Guid("8596f977-843a-44ac-b256-4e522e362151")]
    [ComVisible(true)]
    public interface IDeterminant
    {
        DeterminantInfo CalculateMatrixDeterminant(CalculationMode mode);
    }
}
