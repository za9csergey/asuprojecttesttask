﻿using AsuProjectTestTask.MatrixDeterminant;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace AsuProjectTestTask.Frontend
{
    public partial class Form1 : Form
    {
        private Determinant determinant;
        private double[,] matrix;

        public Form1()
        {
            InitializeComponent();
            SetMatrixVisbility(false);
        }

        private void SetMatrixVisbility(bool visibility)
        {
            MatrixLabel.Visible = visibility;
            MatrixDGV.Visible = visibility;
        }

        private void MatrixDGV_ColumnAdded(object sender, System.Windows.Forms.DataGridViewColumnEventArgs e)
        {
            e.Column.Width = 50;
        }

        private void GenerateMatrixButton_Click(object sender, EventArgs e)
        {
            int matrixSize = ValidateMatrixSizeTextBox();
            if (matrixSize == -1)
            {
                return;
            }

            // create matrix with random values and fill datagridview
            // clear dgv
            MatrixDGV.Rows.Clear();
            MatrixDGV.Columns.Clear();
            MatrixDGV.ColumnCount = matrixSize;
            MatrixDGV.Width = (matrixSize+2)*50;
            MatrixDGV.Height = (matrixSize + 2) * 20;
            SetMatrixVisbility(true);

            matrix = new double[matrixSize, matrixSize];
            Random random = new Random();
            for (int row = 0; row < matrixSize; row++)
            {
                var DGVRow = new DataGridViewRow();
                for (int collumn = 0; collumn < matrixSize; collumn++)
                {
                    matrix[row, collumn] = random.NextDouble();
                    DGVRow.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = matrix[row, collumn]
                    });
                }

                MatrixDGV.Rows.Add(DGVRow);
            }

            determinant = new Determinant(matrix);
        }

        private int ValidateMatrixSizeTextBox()
        {
            if (MatrixSizeTextBox.Text.Equals(String.Empty))
            {
                MessageBox.Show("Enter size of matrix please");
                return -1;
            }

            int matrixSize = Convert.ToInt32(MatrixSizeTextBox.Text);
            if (matrixSize < 2)
            {
                MessageBox.Show("Size of matrix must not less than 2");
                return -1;
            }

            return matrixSize;
        }

        private void MultiThreadButton_Click(object sender, System.EventArgs e)
        {
            UIEnableStatus(false);
            DeterminantInfo determinantInfo = determinant.CalculateMatrixDeterminant(CalculationMode.MultiThread);
            UpdateResultsTable(determinantInfo);
        }

        private void SingleThreadMode_Click(object sender, System.EventArgs e)
        {
            UIEnableStatus(false);
            DeterminantInfo determinantInfo = determinant.CalculateMatrixDeterminant(CalculationMode.SingleThread);
            UpdateResultsTable(determinantInfo);
        }

        private void UpdateResultsTable(DeterminantInfo determinantInfo)
        {
            if (DeterminantDGV.RowCount > 2)
            {
                DeterminantDGV.Rows.Clear();
            }

            DeterminantDGV.Rows.Add(determinantInfo.Determinant, determinantInfo.TimeToCalculate);
            UIEnableStatus(true);
        }

        void UIEnableStatus(bool status)
        {
            MatrixSizeTextBox.Enabled = status;
            generateMatrixButton.Enabled = status;
            SingleThreadMode.Enabled = status;
            MultiThreadButton.Enabled = status;
        }
    }
}
