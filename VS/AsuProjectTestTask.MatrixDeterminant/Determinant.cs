﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace AsuProjectTestTask.MatrixDeterminant
{
    /// <summary>
    /// Enum for calculation mode
    /// </summary>
    [Guid("cd55c7b9-dea0-4825-bf81-324a2c1d8c53")]
    [ComVisible(true)]
    public enum CalculationMode
    {
        SingleThread,
        MultiThread
    }

    /// <summary>
    /// Class to calculate determinant
    /// </summary>
    [Guid("08565cee-682b-47c1-b3db-b47bde292328")]
    [ComVisible(true)]
    public class Determinant : IDeterminant
    {
        private readonly double[,] matrix;  // copy of matrix
        private readonly int size;                   // size of matrix
        private readonly int[] collumnNumbers;       // an array of column numbers
                                            // to add to the summands, using LINQ
        /// <summary>
        /// Creates instance of determinant to calculate it
        /// </summary>
        /// <param name="matrix">square matrix</param>
        public Determinant(double[,] matrix)
        {
            this.matrix = matrix;
            size = matrix.GetLength(0);
            collumnNumbers = Enumerable.Range(0, size).ToArray();
        }

        public Determinant() { }

        /// <summary>
        /// Calculates determinant of input matrix by expansion on the first line
        /// </summary>
        /// <returns>determinant result</returns>

        public DeterminantInfo CalculateMatrixDeterminant(CalculationMode mode)
        {
            Stopwatch watch = Stopwatch.StartNew(); // start stopwatch to measure calculation time
            DeterminantInfo determinantInfo = new DeterminantInfo();

            // if matrix is 2x2 calculate his minor
            if (size == 2)
            {
                Part part = new Part(1, matrix);
                determinantInfo.Determinant = part.ProccesPart();
                watch.Stop();
                determinantInfo.TimeToCalculate = watch.ElapsedMilliseconds;
                return determinantInfo;
            }

            switch (mode)
            {
                case CalculationMode.SingleThread:
                    determinantInfo.Determinant = collumnNumbers.Sum(collumn => 
                        SubMatrix.CalculateSubMatrixDeterminant(matrix, collumn));
                    break;
                case CalculationMode.MultiThread:
                    SYSTEM_INFO sysInfo = new SYSTEM_INFO();
                    GetSystemInfo(ref sysInfo);
                    ThreadPool.SetMaxThreads((int)sysInfo.numberOfProcessors, (int) sysInfo.numberOfProcessors);
                    determinantInfo.Determinant = collumnNumbers.AsParallel().Sum(collumn =>
                        SubMatrix.CalculateSubMatrixDeterminant(matrix, collumn));
                    break;
            }

            // stop stopwatch and save data
            watch.Stop();
            determinantInfo.TimeToCalculate = watch.ElapsedMilliseconds;
            return determinantInfo;
        }

        [DllImport("kernel32")]
        public static extern void GetSystemInfo(ref SYSTEM_INFO SystemInfo);
    }
}
