﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AsuProjectTestTask.MatrixDeterminant;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AsuProjectTestTask.UnitTests
{
    [TestClass]
    public class TestMatrixDeterminant
    {
        [TestMethod]
        public void Test2x2Matrix()
        {
            double[,] matrix = {
                {1.0, 4.0},
                {3.0, 2.0}
            };
            Part part = new Part(2, matrix);
            Assert.AreEqual(-20.0, part.ProccesPart());
        }

        [TestMethod]
        public void TestSubMatrix()
        {
            double[,] matrix = {
                {1.0, 4.0, 5.0},
                {3.0, 2.0, 1.0},
                {7.0, 5.0, 4.0}
            };
            double[,] subMatrix = SubMatrix.Create(matrix, 1);
            Assert.AreEqual(3.0, subMatrix[0, 0]);
            Assert.AreEqual(1.0, subMatrix[0, 1]);
            Assert.AreEqual(7.0, subMatrix[1, 0]);
            Assert.AreEqual(4.0, subMatrix[1, 1]);
        }

        [TestMethod]
        public void TestDeterminator5x5()
        {
            double[,] matrix = {
                {1.0, 2.0, 4.0, 5.0, 8.0},
                {9.0, 5.0, 2.0, 1.0, 6.0},
                {7.0, 8.0, 3.0, 2.0, 1.0},
                {3.0, 6.0, 3.0, 5.0, 2.0},
                {1.0, 2.0, 4.0, 5.0, 9.0}
            };

            Determinant determinant = new Determinant(matrix);
            DeterminantInfo result = determinant.CalculateMatrixDeterminant(CalculationMode.SingleThread);
            Assert.AreEqual(275.0, result.Determinant);
            Console.WriteLine(result.TimeToCalculate);
            result = determinant.CalculateMatrixDeterminant(CalculationMode.MultiThread);
            Assert.AreEqual(275.0, result.Determinant);
            Console.WriteLine(result.TimeToCalculate);
        }

        [TestMethod]
        public void TestGuid()
        {
            for(int i = 0; i < 10; i++)
            {
                string guid = Guid.NewGuid().ToString();
                Console.WriteLine(guid);
            }

        }
    }

}
